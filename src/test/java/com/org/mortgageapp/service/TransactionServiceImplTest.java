package com.org.mortgageapp.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.org.mortgageapp.entity.User;
import com.org.mortgageapp.entity.UserAccount;
import com.org.mortgageapp.repository.TransactionRepository;
import com.org.mortgageapp.repository.UserAccountRepository;
import com.org.mortgageapp.repository.UserRepository;

@SpringBootTest
class TransactionServiceImplTest {
	
	@Mock
	TransactionRepository transactionRepository;
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	UserAccountRepository userAccountRepository;
	
	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;
	
	

	@Test
	public void testperformTransaction() {
		List<User> userList = new ArrayList<>();
		User user = new User();
		user.setFirstName("firstName");
		userList.add(user);
		Mockito.when(userRepository.findAll()).thenReturn(userList);
		List<UserAccount> userAccntList = new ArrayList<>();
		UserAccount userAccnt = new UserAccount();
		userAccnt.setAccountId("12345");
		userAccnt.setAccountType("CURRENT");
		Double doo = new Double(2000);
		userAccnt.setBalance(doo);
		
		userAccntList.add(userAccnt);
		Mockito.when(userAccountRepository.findByUser(Mockito.any(User.class))).thenReturn(userAccntList );
		transactionServiceImpl.performTransaction();
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}

}

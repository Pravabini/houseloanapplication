package com.org.mortgageapp.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.org.mortgageapp.dto.RegisterRequestDto;
import com.org.mortgageapp.dto.RegisterResponceDto;
import com.org.mortgageapp.entity.User;
import com.org.mortgageapp.entity.UserAccount;
import com.org.mortgageapp.entity.UserOccupation;
import com.org.mortgageapp.entity.UserProperty;
import com.org.mortgageapp.exception.AgeException;
import com.org.mortgageapp.exception.DepositAmmountError;
import com.org.mortgageapp.repository.UserAccountRepository;
import com.org.mortgageapp.repository.UserOccupationRepository;
import com.org.mortgageapp.repository.UserPropertyRepository;
import com.org.mortgageapp.repository.UserRepository;

@SpringBootTest
class RegistrationServiceImplTest {
	@Mock
	UserRepository userRepository;

	@Mock
	UserAccountRepository userAccountRepository;

	@Mock
	UserOccupationRepository userOccupationRepository;

	@Mock
	UserPropertyRepository userPropertyRepository;

	@InjectMocks
	UserServiceImpl userServiceImpl;

	RegisterRequestDto registerRequestDto;
	UserProperty userProperty;
	UserOccupation userOccupation;
	UserAccount userAccount;
	User user;

	@BeforeEach
	public void setup() {
		registerRequestDto = new RegisterRequestDto();
		registerRequestDto.setFirstName("prava");
		registerRequestDto.setDateOfBirth("1995-01-01");
		registerRequestDto.setJobDate("2000-10-10");
		registerRequestDto.setPropertyAmount(100000.0);
		registerRequestDto.setDepositAmount(10000.0);
		user = new User();
		userProperty = new UserProperty();
		userOccupation = new UserOccupation();
		userAccount = new UserAccount();	
		user.setUserId("1");
		user.setPassword("prava");

	}

	@Test
	void testUserRegistration() throws AgeException, DepositAmmountError {

		Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
		Mockito.when(userPropertyRepository.save(Mockito.any(UserProperty.class))).thenReturn(userProperty);
		Mockito.when(userOccupationRepository.save(Mockito.any(UserOccupation.class))).thenReturn(userOccupation);
		Mockito.when(userAccountRepository.save(Mockito.any(UserAccount.class))).thenReturn(userAccount);
		RegisterResponceDto message = userServiceImpl.userRegistration(registerRequestDto);
		Assertions.assertEquals(700, message.getStatusCode());
	}
	
	@Test
	void testUserRegistrationAgeException() throws AgeException, DepositAmmountError {
		registerRequestDto.setDateOfBirth("2010-01-01");
		  Assertions.assertThrows(AgeException.class,
	                () -> userServiceImpl.userRegistration(registerRequestDto));
	}

	@Test
	void testUserRegistrationDepositAmmountError() throws AgeException, DepositAmmountError {
		registerRequestDto.setDepositAmount(200000.0);
		  Assertions.assertThrows(DepositAmmountError.class,
	                () -> userServiceImpl.userRegistration(registerRequestDto));
		  }
	
	@Test
	void testUserRegistrationDepositAmmountError1() throws AgeException, DepositAmmountError {
		registerRequestDto.setPropertyAmount(10000.0);
		  Assertions.assertThrows(DepositAmmountError.class,
	                () -> userServiceImpl.userRegistration(registerRequestDto));
		  }
}

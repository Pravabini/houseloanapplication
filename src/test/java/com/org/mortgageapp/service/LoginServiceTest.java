package com.org.mortgageapp.service;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.org.mortgageapp.dto.LoginRequestDto;
import com.org.mortgageapp.dto.LoginResponseDto;
import com.org.mortgageapp.entity.User;
import com.org.mortgageapp.exception.UserExistsErrorException;
import com.org.mortgageapp.exception.UserPasswordErrorException;
import com.org.mortgageapp.repository.UserRepository;

@SpringBootTest
class LoginServiceTest {
	@Mock
	UserRepository userRepository;
	@InjectMocks
	LoginServiceImpl LoginServiceImpl;

	LoginResponseDto userLoginResponseDto;

	LoginRequestDto loginRequestDto;
	User user;

	@BeforeEach
	public void setup() {
		userLoginResponseDto = new LoginResponseDto();
		userLoginResponseDto.setStatusCode(701);
		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setUserId("suppi");
		loginRequestDto.setPassword("1234");

		user=new  User();
	}
	
	@Test
	void userLoginTest() throws UserPasswordErrorException, UserExistsErrorException {
		 Mockito.when(userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString())).thenReturn(Optional.of(user));
		  Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		  LoginResponseDto result = LoginServiceImpl.userLogin(loginRequestDto);
		 Assertions.assertEquals(701, result.getStatusCode());
	}

	@Test
	void userLoginTestUserPasswordErrorException() throws UserPasswordErrorException, UserExistsErrorException {
		 Mockito.when(userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString())).thenReturn(Optional.empty());
		  Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		  Assertions.assertThrows(UserPasswordErrorException.class,
	                () ->  LoginServiceImpl.userLogin(loginRequestDto));
	}
	
	@Test
	void userLoginTestUserExistsErrorException() throws UserPasswordErrorException, UserExistsErrorException {
		 Mockito.when(userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString())).thenReturn(Optional.empty());
		  Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.empty());
		  Assertions.assertThrows(UserExistsErrorException.class,
	                () ->  LoginServiceImpl.userLogin(loginRequestDto));
	}
}

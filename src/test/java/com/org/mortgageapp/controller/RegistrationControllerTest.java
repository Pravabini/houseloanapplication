package com.org.mortgageapp.controller;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.org.mortgageapp.dto.RegisterRequestDto;
import com.org.mortgageapp.dto.RegisterResponceDto;
import com.org.mortgageapp.exception.AgeException;
import com.org.mortgageapp.exception.DepositAmmountError;
import com.org.mortgageapp.service.UserServiceImpl;

@SpringBootTest
class RegistrationControllerTest {

	@InjectMocks
	UserController userController;

	@Mock
	UserServiceImpl userServiceImpl;

	RegisterResponceDto responseDto;

	@BeforeEach
	public void setup() {
		responseDto = new RegisterResponceDto();
		responseDto.setMsg("success");
	}

	@Test
	void testUserRegistration() throws AgeException, DepositAmmountError {
		RegisterRequestDto registerRequestDto = new RegisterRequestDto();
		Mockito.when(userServiceImpl.userRegistration(registerRequestDto)).thenReturn(responseDto);
		ResponseEntity<RegisterResponceDto> response = userController.userRegistration(registerRequestDto);
		Assertions.assertEquals("success", response.getBody().getMsg());

  }

}

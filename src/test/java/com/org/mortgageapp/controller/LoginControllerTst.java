package com.org.mortgageapp.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.org.mortgageapp.dto.LoginRequestDto;
import com.org.mortgageapp.dto.LoginResponseDto;
import com.org.mortgageapp.exception.UserExistsErrorException;
import com.org.mortgageapp.exception.UserPasswordErrorException;
import com.org.mortgageapp.service.LoginService;

@SpringBootTest
class LoginControllerTst {
	@Mock
	LoginService loginService;
	
	@InjectMocks
	LoginController loginController;
	
	LoginResponseDto userLoginResponseDto;

	LoginRequestDto loginRequestDto;

	@BeforeEach
	public void setup() {
		userLoginResponseDto = new LoginResponseDto();
		userLoginResponseDto.setStatusCode(701);
		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setUserId("suppi");
		loginRequestDto.setPassword("1234");

	}

	
	@Test
	void LoginControllerTest() throws UserPasswordErrorException, UserExistsErrorException {
		Mockito.when(loginService.userLogin(Mockito.any(LoginRequestDto.class))).thenReturn(userLoginResponseDto);
		ResponseEntity<LoginResponseDto> entity = loginController.checkLoginByUserId(loginRequestDto);
		Assertions.assertEquals(userLoginResponseDto.getStatusCode(), entity.getBody().getStatusCode());
		
	}

}

package com.org.mortgageapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.mortgageapp.dto.RegisterRequestDto;
import com.org.mortgageapp.dto.RegisterResponceDto;
import com.org.mortgageapp.exception.AgeException;
import com.org.mortgageapp.exception.DepositAmmountError;
import com.org.mortgageapp.service.UserService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@PostMapping("/")
	public ResponseEntity<RegisterResponceDto> userRegistration(@RequestBody RegisterRequestDto userRequestDto) throws AgeException, DepositAmmountError{
		logger.info("inside userRegistration method");
		RegisterResponceDto message = userService.userRegistration(userRequestDto);
		logger.info("leaving userRegistration method");
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	

}

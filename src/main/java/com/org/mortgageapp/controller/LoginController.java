package com.org.mortgageapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.mortgageapp.dto.LoginRequestDto;
import com.org.mortgageapp.dto.LoginResponseDto;
import com.org.mortgageapp.exception.UserExistsErrorException;
import com.org.mortgageapp.exception.UserPasswordErrorException;
import com.org.mortgageapp.service.LoginService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	LoginService loginService;

	@PostMapping(value = "/login")
	public ResponseEntity<LoginResponseDto> checkLoginByUserId(@RequestBody LoginRequestDto loginRequestDto)
			throws UserExistsErrorException, UserPasswordErrorException {
		logger.info("Inside UserController of customerLogin method ");
		return new ResponseEntity<>(loginService.userLogin(loginRequestDto), HttpStatus.ACCEPTED);
	}
}

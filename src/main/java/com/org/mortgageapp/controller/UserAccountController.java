package com.org.mortgageapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.mortgageapp.dto.UserAccountResponseDto;
import com.org.mortgageapp.dto.UserTransactionHistoryDto;
import com.org.mortgageapp.exception.UserAccountException;
import com.org.mortgageapp.service.UserAccountService;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/users")
public class UserAccountController {
	@Autowired
	UserAccountService userAccountService;

	Logger logger = LoggerFactory.getLogger(UserAccountController.class);

	@GetMapping("/{userid}/useraccounts")
	@ApiOperation("getting the account information")
	public ResponseEntity<UserAccountResponseDto> getAllUserAccounts(@PathVariable("userid") String userId) throws UserAccountException {

		return new ResponseEntity<>(userAccountService.getAllUserAccounts(userId), HttpStatus.OK);

	}
	
	@GetMapping("/{accountid}/transaction-history")
    @ApiOperation("getting the all details of transaction information")
    public  ResponseEntity<UserTransactionHistoryDto> getAllTransactions(@PathVariable("accountid") String accountId ) throws UserAccountException {
        
                 return new ResponseEntity<>(userAccountService.getAllTransactions(accountId), HttpStatus.OK);
 

 

    }

}

package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequestDto {
	
	  private String userId;
	  private String password;
}

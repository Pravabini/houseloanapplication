package com.org.mortgageapp.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserTransactionHistoryDto extends ResponseDto{
	private String accountId;
	private List<HistoryDto> transactionList;

}

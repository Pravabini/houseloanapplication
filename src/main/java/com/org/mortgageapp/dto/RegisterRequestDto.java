package com.org.mortgageapp.dto;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterRequestDto {
	
	private String title;
	 private String firstName;
	    private String middleName;
	    private String surname;
	    private double phoneNumber;
	    private String dateOfBirth;
	    private String designation;  
	    private String contractType;   
	    private String jobStatus;	   
	    private String jobDate;
	    private Double propertyAmount;
	    private Double depositAmount;
	    @Email
	    @NotBlank
	    private String emailId;


}

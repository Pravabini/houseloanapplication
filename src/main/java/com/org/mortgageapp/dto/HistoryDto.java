package com.org.mortgageapp.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class HistoryDto {
	 private String comment;
	    private LocalDateTime dateTime;
	    private Double amount;
	    private Double balance;
}

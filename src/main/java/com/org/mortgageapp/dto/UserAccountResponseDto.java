package com.org.mortgageapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserAccountResponseDto extends ResponseDto {
		
	   private String currentAccountId;
	     private Double currentBalance;
	    private String mortageAccountId;
	    private Double mortageBalance;
	    

}

package com.org.mortgageapp.constant;

public class AppConstants {
	
	
	public static final String SUCCESS = "SUCCESS";
	
	public static final String FAILURE = "FAILED";
	
	public static final String CURRENT_ACCOUNT = "CURRENT";
	
	public static final String MORTAGE_ACCOUNT = "MORTGAGE";

	public static final String DEBIT = "DEBIT";

	public static final String CREDIT = "CREDIT";




}

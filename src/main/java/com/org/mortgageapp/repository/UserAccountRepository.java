package com.org.mortgageapp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgageapp.entity.User;
import com.org.mortgageapp.entity.UserAccount;

@Repository
public interface UserAccountRepository extends CrudRepository<UserAccount, String> {

	List<UserAccount> findByUser(User user);


}

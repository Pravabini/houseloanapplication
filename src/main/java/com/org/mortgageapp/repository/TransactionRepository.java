package com.org.mortgageapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgageapp.entity.TransactionHistory;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionHistory, Long> {

	List<TransactionHistory> findByAccountId(String accountId);

	List<TransactionHistory> findTop5ByAccountIdOrderByTransactionIdDesc(String accountId);

	

}

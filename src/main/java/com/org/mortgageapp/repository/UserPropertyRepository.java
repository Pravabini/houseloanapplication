package com.org.mortgageapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgageapp.entity.UserProperty;

@Repository
public interface UserPropertyRepository extends CrudRepository<UserProperty, Long> {


}

package com.org.mortgageapp.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgageapp.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

	Optional<User> findByUserIdAndPassword(String userId, String password);


}

package com.org.mortgageapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.org.mortgageapp.entity.UserOccupation;

@Repository
public interface UserOccupationRepository extends CrudRepository<UserOccupation, Long> {


}

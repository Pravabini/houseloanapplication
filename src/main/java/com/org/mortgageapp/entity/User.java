package com.org.mortgageapp.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="user")
public class User {
    
    @Id
    private String userId;
    private String title;
    private String firstName;
    private String middleName;
    private String surname;
    private double phoneNumber;
    @Column(unique=true)
    private String emailId;
    private String password;
    private LocalDate dateOfBirth;
    private String operationType;
    
    
    
}
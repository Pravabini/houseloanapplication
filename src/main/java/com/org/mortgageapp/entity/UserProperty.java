package com.org.mortgageapp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="user_property")
public class UserProperty {
    
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long propertId;
    
    private Double propertyAmount;
    
    @ManyToOne
	@JoinColumn(name="user_id")
	private User user;
    
    private Double depositAmount;
    
    private String propertyDetail;
    
    
}
    
    
    
    
    
    

 
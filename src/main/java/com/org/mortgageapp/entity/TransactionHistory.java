package com.org.mortgageapp.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="transaction_history")
public class TransactionHistory {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long transactionId;
	
	private double transactionAmount;
	
	private LocalDateTime transactionDate;
	
	private String comment;
	
	private String transactionType;
	
	private String accountId;
	
	  private Double balance;
	
	
	
	

}

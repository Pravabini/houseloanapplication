package com.org.mortgageapp.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
@Data
@Entity
@Table(name="user_account")
public class UserAccount {
    
    @Id
    private String accountId;
    
    private Double balance;
    private LocalDateTime dateTime;
    
    @ManyToOne
   	@JoinColumn(name="user_id")
   	private User user;
    
    private String accountType;
    
    
    
    
    
}
package com.org.mortgageapp.exception;

public class UserPasswordErrorException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserPasswordErrorException(String message) {
		super(message);
	}

}

package com.org.mortgageapp.exception;

public class UserAccountException extends Exception {
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserAccountException(String message) {
        super(message);
    }

}

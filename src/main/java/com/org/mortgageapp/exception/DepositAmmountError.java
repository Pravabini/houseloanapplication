package com.org.mortgageapp.exception;

public class DepositAmmountError extends Exception {
	
	private static final long serialVersionUID = 1L;

	public DepositAmmountError(String message) {
		super(message);
	}

}

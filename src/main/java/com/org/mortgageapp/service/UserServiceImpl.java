package com.org.mortgageapp.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.mortgageapp.dto.RegisterRequestDto;
import com.org.mortgageapp.dto.RegisterResponceDto;
import com.org.mortgageapp.entity.User;
import com.org.mortgageapp.entity.UserAccount;
import com.org.mortgageapp.entity.UserOccupation;
import com.org.mortgageapp.entity.UserProperty;
import com.org.mortgageapp.exception.AgeException;
import com.org.mortgageapp.exception.DepositAmmountError;
import com.org.mortgageapp.repository.UserAccountRepository;
import com.org.mortgageapp.repository.UserOccupationRepository;
import com.org.mortgageapp.repository.UserPropertyRepository;
import com.org.mortgageapp.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserAccountRepository userAccountRepository;

	@Autowired
	UserOccupationRepository userOccupationRepository;

	@Autowired
	UserPropertyRepository userPropertyRepository;

	@Autowired
	UserRepository userRepository;

	@Override
	public RegisterResponceDto userRegistration(RegisterRequestDto userRequestDto)
			throws AgeException, DepositAmmountError {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(userRequestDto.getDateOfBirth(), formatter);

		if (userRequestDto.getPropertyAmount() < 100000)
			throw new DepositAmmountError("property should more than 100000");

		if (LocalDate.now().getYear() - date.getYear() < 18)
			throw new AgeException("Sorry, your loan request has been rejected! The applicant should be minimum 18 years old in order to apply for the mortgage loan.");

		if (userRequestDto.getPropertyAmount() < userRequestDto.getDepositAmount())
			throw new DepositAmmountError(
					"you have more deposite ammount than loan ammount");

		String password = RandomStringUtils.randomAlphanumeric(7);

		String userId = RandomStringUtils.randomAlphanumeric(5);

		String mortgageAccountId = RandomStringUtils.randomNumeric(5);

		String currentAccountId = RandomStringUtils.randomNumeric(5);

		User user = new User();
		BeanUtils.copyProperties(userRequestDto, user);
		user.setUserId(userId);	
		user.setPassword(password);
		User user1 = userRepository.save(user);

		UserProperty userProperty = new UserProperty();
		userProperty.setDepositAmount(userRequestDto.getDepositAmount());
		userProperty.setPropertyAmount(userRequestDto.getPropertyAmount());
		userProperty.setUser(user1);
		userPropertyRepository.save(userProperty);

		UserOccupation occupation = new UserOccupation();
		DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateTime = LocalDate.parse(userRequestDto.getJobDate(), formatter1);
		occupation.setJobDate(dateTime);
		BeanUtils.copyProperties(userRequestDto, occupation);
		occupation.setUser(user1);

		UserAccount userAccount = new UserAccount();

		userAccount.setAccountId(mortgageAccountId);
		userAccount.setAccountType("Mortgage");
		userAccount.setBalance(-(userRequestDto.getPropertyAmount() - userRequestDto.getDepositAmount()));
		userAccount.setDateTime(LocalDateTime.now());
		userAccount.setUser(user1);

		userAccountRepository.save(userAccount);

		userAccount.setAccountId(currentAccountId);
		userAccount.setAccountType("Current");
		userAccount.setBalance(userRequestDto.getPropertyAmount() - userRequestDto.getDepositAmount());
		userAccount.setDateTime(LocalDateTime.now());
		userAccount.setUser(user1);

		userAccountRepository.save(userAccount);

		RegisterResponceDto registerResponceDto = new RegisterResponceDto();
		registerResponceDto.setCurrentaccountId(currentAccountId);
		registerResponceDto.setMordgageaccountId(mortgageAccountId);
		registerResponceDto.setFirstName(userRequestDto.getFirstName());
		registerResponceDto.setMsg("Register success");
		registerResponceDto.setStatusCode(700);
		registerResponceDto.setUserId(userId);
		registerResponceDto.setPassword(password);
		return registerResponceDto;
	}

}

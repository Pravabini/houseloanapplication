package com.org.mortgageapp.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.mortgageapp.dto.LoginRequestDto;
import com.org.mortgageapp.dto.LoginResponseDto;
import com.org.mortgageapp.entity.User;
import com.org.mortgageapp.exception.UserExistsErrorException;
import com.org.mortgageapp.exception.UserPasswordErrorException;
import com.org.mortgageapp.repository.UserRepository;

@Service
public class LoginServiceImpl implements LoginService{
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
@Autowired
UserRepository userRepository;


	@Override
	public LoginResponseDto userLogin(LoginRequestDto loginRequestDto) throws UserPasswordErrorException, UserExistsErrorException
	{
		logger.info("inside customerLogin method of CustomerServiceImpl class");
		Optional<User> userDetail = userRepository.findByUserIdAndPassword(loginRequestDto.getUserId(),loginRequestDto.getPassword());
		if (!userDetail.isPresent()) {

			Optional<User> userDetail1 = userRepository.findById(loginRequestDto.getUserId());
			if (!userDetail1.isPresent())
             throw new UserExistsErrorException("user not found");

			throw new UserPasswordErrorException("incorrect password");
		}

		
		LoginResponseDto loginResponseDto = new LoginResponseDto();
		loginResponseDto.setUserId(loginRequestDto.getUserId());
		loginResponseDto.setMsg("login success");
		loginResponseDto.setStatusCode(701);
		
		
		return loginResponseDto;
	}


}

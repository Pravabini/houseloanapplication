package com.org.mortgageapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.mortgageapp.dto.HistoryDto;
import com.org.mortgageapp.dto.UserAccountResponseDto;
import com.org.mortgageapp.dto.UserTransactionHistoryDto;
import com.org.mortgageapp.entity.TransactionHistory;
import com.org.mortgageapp.entity.User;
import com.org.mortgageapp.entity.UserAccount;
import com.org.mortgageapp.exception.UserAccountException;
import com.org.mortgageapp.repository.TransactionRepository;
import com.org.mortgageapp.repository.UserAccountRepository;
import com.org.mortgageapp.repository.UserRepository;

@Service
public class UserAccountServiceImpl implements UserAccountService {

	@Autowired
	UserAccountRepository userAccountRepository;
	@Autowired
	UserRepository userRepository;

	@Autowired
	TransactionRepository transactionRepository;
	Logger logger = LoggerFactory.getLogger(UserAccountServiceImpl.class);

	@Override
	public UserAccountResponseDto getAllUserAccounts(String userId) throws UserAccountException {
		Optional<User> user = userRepository.findById(userId);
		if (!user.isPresent()) {
			throw new UserAccountException("User  Not Present");
		}
		List<UserAccount> userAccountList = userAccountRepository.findByUser(user.get());

		if (userAccountList.isEmpty()) {
			throw new UserAccountException("User Account Not Present");
		}
		UserAccountResponseDto userAccountResponseDto = new UserAccountResponseDto();
		for (UserAccount userAccount : userAccountList) {

			if (userAccount.getAccountType().equalsIgnoreCase("Current")) {

				userAccountResponseDto.setCurrentAccountId(userAccount.getAccountId());
				userAccountResponseDto.setCurrentBalance(userAccount.getBalance());

			} else if (userAccount.getAccountType().equalsIgnoreCase("Mortgage")) {
				userAccountResponseDto.setMortageAccountId(userAccount.getAccountId());
				userAccountResponseDto.setMortageBalance(userAccount.getBalance());

			}

		}
		userAccountResponseDto.setStatusCode(611);
		userAccountResponseDto.setMsg("UserAccountDetail");
		return userAccountResponseDto;

	}

	@Override
	public UserTransactionHistoryDto getAllTransactions(String accountId) throws UserAccountException {
		Optional<UserAccount> userAccount = userAccountRepository.findById(accountId);
		if (!userAccount.isPresent())
			throw new UserAccountException("User  Not Present");

		List<TransactionHistory> trsanctionHist = transactionRepository.findTop5ByAccountIdOrderByTransactionIdDesc(accountId);
		if (trsanctionHist.isEmpty())
			throw new UserAccountException("Transaction History Not Present");

		UserTransactionHistoryDto userTransactionHistoryDto = new UserTransactionHistoryDto();
		List<HistoryDto> transactionList = new ArrayList<>();
		for (TransactionHistory transactionHistory : trsanctionHist) {
			HistoryDto historyDto = new HistoryDto();
			historyDto.setComment(transactionHistory.getComment());
			historyDto.setAmount(transactionHistory.getTransactionAmount());
			historyDto.setDateTime(transactionHistory.getTransactionDate());
			historyDto.setBalance(transactionHistory.getBalance());
			//BeanUtils.copyProperties(transactionHistory, historyDto);
			transactionList.add(historyDto);

		}

		userTransactionHistoryDto.setTransactionList(transactionList);
		userTransactionHistoryDto.setStatusCode(900);
		userTransactionHistoryDto.setMsg("taransaction history");
		userTransactionHistoryDto.setAccountId(accountId);
		return userTransactionHistoryDto;

	}

}

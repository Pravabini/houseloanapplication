package com.org.mortgageapp.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.mortgageapp.constant.AppConstants;
import com.org.mortgageapp.entity.TransactionHistory;
import com.org.mortgageapp.entity.User;
import com.org.mortgageapp.entity.UserAccount;
import com.org.mortgageapp.repository.TransactionRepository;
import com.org.mortgageapp.repository.UserAccountRepository;
import com.org.mortgageapp.repository.UserRepository;

@Service
public class TransactionServiceImpl {
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	
	public void performTransaction() {
		List<User> userList = (List<User>) userRepository.findAll();
		for (User user : userList) {
			List<UserAccount> userAccntList = (List<UserAccount>) userAccountRepository.findByUser(user);
			
			for (UserAccount userAccnt : userAccntList) {
				String transactionStatus = performTransactionByAccntIdAndAccountType(userAccnt.getAccountId(), userAccnt.getAccountType(),userAccnt.getBalance());
				if (AppConstants.SUCCESS.equalsIgnoreCase(transactionStatus)) {
					if (userAccnt.getAccountType().equalsIgnoreCase(AppConstants.MORTAGE_ACCOUNT)) {
					userAccnt.setBalance(userAccnt.getBalance() + 200);
					} else {
						userAccnt.setBalance(userAccnt.getBalance() - 200);
					}
					userAccnt.setDateTime(LocalDateTime.now());
					userAccountRepository.save(userAccnt);
				}
			}
		}
		
	}
	
	public String performTransactionByAccntIdAndAccountType(String accountId, String accountType, Double balance) {
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setTransactionAmount(200);
		transactionHistory.setTransactionDate(LocalDateTime.now());
		
		if(AppConstants.CURRENT_ACCOUNT.equalsIgnoreCase(accountType)) {
		transactionHistory.setComment("debited from " + accountId );
		transactionHistory.setTransactionType(AppConstants.DEBIT);
		transactionHistory.setBalance(balance-200);
		}
		else {
			transactionHistory.setComment("credited to " + accountId );
			transactionHistory.setTransactionType(AppConstants.CREDIT);
			transactionHistory.setBalance(balance+200);
		}

		transactionHistory.setAccountId(accountId);
		transactionRepository.save(transactionHistory);
		return AppConstants.SUCCESS;
	}

}

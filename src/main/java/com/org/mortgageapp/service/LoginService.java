package com.org.mortgageapp.service;

import com.org.mortgageapp.dto.LoginRequestDto;
import com.org.mortgageapp.dto.LoginResponseDto;
import com.org.mortgageapp.exception.UserExistsErrorException;
import com.org.mortgageapp.exception.UserPasswordErrorException;

public interface LoginService {

	LoginResponseDto userLogin(LoginRequestDto loginRequestDto) throws UserPasswordErrorException, UserExistsErrorException;

}

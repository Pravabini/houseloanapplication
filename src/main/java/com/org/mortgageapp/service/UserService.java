package com.org.mortgageapp.service;

import com.org.mortgageapp.dto.RegisterRequestDto;
import com.org.mortgageapp.dto.RegisterResponceDto;
import com.org.mortgageapp.exception.AgeException;
import com.org.mortgageapp.exception.DepositAmmountError;

public interface UserService {

	RegisterResponceDto userRegistration(RegisterRequestDto userRequestDto) throws AgeException, DepositAmmountError;
	

}

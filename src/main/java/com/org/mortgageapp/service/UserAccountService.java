package com.org.mortgageapp.service;

import com.org.mortgageapp.dto.UserAccountResponseDto;
import com.org.mortgageapp.dto.UserTransactionHistoryDto;
import com.org.mortgageapp.exception.UserAccountException;

public interface UserAccountService {
    public UserAccountResponseDto getAllUserAccounts(String userId)throws UserAccountException;
    public UserTransactionHistoryDto getAllTransactions( String accountId) throws UserAccountException;


}

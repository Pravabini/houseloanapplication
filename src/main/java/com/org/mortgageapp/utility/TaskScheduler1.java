package com.org.mortgageapp.utility;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.org.mortgageapp.service.TransactionServiceImpl;

@Component
public class TaskScheduler1 {
	
	@Autowired
	private TransactionServiceImpl transactionServiceImpl;
	
	@Scheduled(fixedRate = 60000)
	public void taskScheduling() {
		System.out.println("date time "+LocalDateTime.now());
		transactionServiceImpl.performTransaction();
		System.out.println("date time "+LocalDateTime.now());

	}

}
